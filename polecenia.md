http://nodejs.org/ 

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2

git clone https://bitbucket.org/ev45ive/demo-angular.git

cd demo-angular

# Zainstaluj bibtioteki
npm i 

# Uruchom lokalnie
npm serve -o

# Schowaj lokalne zmiany
git stash

# Pobierz i nadpisz 
git pull --force

