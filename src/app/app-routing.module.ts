import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumSearchComponent } from './music-search/pages/album-search/album-search.component';
import { AlbumDetailsComponent } from './music-search/pages/album-details/album-details.component';


const routes: Routes = [
  {
    path: '',
    component: AlbumSearchComponent
  },
  {
    path: 'album/:album_id',
    component: AlbumDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: true,
    useHash: true
  })],
  exports: [RouterModule]
}) 
export class AppRoutingModule { }
