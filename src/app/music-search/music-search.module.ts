import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumSearchComponent } from './pages/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AlbumDetailsComponent } from './pages/album-details/album-details.component';
import { DurationPipe } from './shared/duration.pipe';


@NgModule({
  declarations: [
    AlbumSearchComponent, 
    SearchFormComponent, 
    SearchResultsComponent, 
    AlbumCardComponent, AlbumDetailsComponent, DurationPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MusicSearchRoutingModule
  ],
  exports: [AlbumSearchComponent,AlbumDetailsComponent]
})
export class MusicSearchModule { }
