import { Component, OnInit, Input } from '@angular/core';
import { Album } from 'src/app/models/search.models';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  @Input()
  results:Album[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
