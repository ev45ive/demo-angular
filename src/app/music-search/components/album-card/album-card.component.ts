import { Component, OnInit, Input } from '@angular/core';
import { Album } from 'src/app/models/search.models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.css']
})
export class AlbumCardComponent implements OnInit {

  @Input()
  album: Album

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  selectDetails() {
    this.router.navigate(['/album', this.album.id])
  }

}
