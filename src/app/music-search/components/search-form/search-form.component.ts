import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { filter, debounceTime } from 'rxjs/operators'
import { FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  searchForm = new FormGroup({
    query: new FormControl('batman'),
    type: new FormControl('album'),
  })

  @Input()
  // query = 'batman'
  set query(v) {
    this.searchForm.get('query').setValue(v)
  }

  // ngOnChange() { }
  // ngDoCheck() { }
  // ngAfterViewInit() { }
  // ngOnDestroy() { }
  ngOnInit(): void { }

  @Output()
  queryChange = new EventEmitter<string>();

  constructor() {
    (window as any).form = this.searchForm

    const queryField = this.searchForm.get('query')

    queryField.valueChanges
      .pipe(
        filter(query => query.length >= 3),
        debounceTime(500)
      )
      .subscribe(query => {
        this.queryChange.emit(query)
      })
  }



  search() {
    this.queryChange.emit(this.searchForm.get('query').value)
  }

}
