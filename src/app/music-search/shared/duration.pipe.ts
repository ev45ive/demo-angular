import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]) {
    const date = new Date(value).toUTCString()
    return date;
    // return `${date.getUTCHours()}:${date.getUTCMinutes()}:${date.getUTCSeconds()}`
  }

}
