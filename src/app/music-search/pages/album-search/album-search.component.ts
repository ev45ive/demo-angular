import { Component, OnInit, Inject } from '@angular/core';
import { Album, AlbumsSearchResults } from 'src/app/models/search.models';
import { HttpClient } from '@angular/common/http'
import { AuthService } from 'src/app/core/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.css']
})
export class AlbumSearchComponent implements OnInit {
  message = ''

  foundAlbums: Partial<Album>[] = [
    {
      id: '123',
      name: 'Lubie placki 123', images: [
        {
          url: 'https://www.placecage.com/c/300/300',
          height: 200, width: 200
        }
      ]
    }
  ]

  constructor(
    // @Inject(HttpClient)
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParam => {
      this.query = (queryParam.get('query') || 'batman')

      this.http.get<AlbumsSearchResults>('https://api.spotify.com/v1/search', {
        params: {
          type: 'album',
          query: this.query
        },
        headers: {
          Authorization: `Bearer ${this.auth.getToken()}`
        }
      }).subscribe(resp => {
        this.foundAlbums = resp.albums.items;
      }, error => {
        this.message = (error.error.error.message);
      });
    })
  }

  query = 'batman'

  search(query = 'batman') {

    this.router.navigate(['/'], {
      queryParams: {
        query
      }
    })
  }
}
