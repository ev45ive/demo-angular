import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Album, Track } from 'src/app/models/search.models';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.css']
})
export class AlbumDetailsComponent implements OnInit {

  album: Album
  message = '';
  volume = 0.2;

  @ViewChild('audioRef')
  audioRef: ElementRef<HTMLAudioElement>

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      const album_id = paramMap.get('album_id')

      this.http.get<Album>(`https://api.spotify.com/v1/albums/${album_id}`, {
        headers: {
          Authorization: `Bearer ${this.auth.getToken()}`
        }
      }).subscribe(resp => {
        this.album = resp
      }, error => {
        this.message = (error.error.error.message);
      });

    })
  }

  track: Track

  play(track: Track) {
    this.track = track
    // this.audioRef.nativeElement.src = track.preview_url
    setTimeout(() => {
      this.audioRef.nativeElement?.play()
    })
  }

}
