import { Injectable, Optional } from '@angular/core';
import { HttpParams } from '@angular/common/http';

interface AuthConfig {
  url: string;
  client_id: string;
  response_type: 'token';
  redirect_uri: string;
}


const exampleConfig: AuthConfig = {
  client_id: '336db8fcd75a49ccb0b6d6d4d902793b',
  response_type: 'token',
  url: 'https://accounts.spotify.com/authorize',
  redirect_uri: 'http://localhost:4200/'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private config: AuthConfig = exampleConfig

  constructor() {
    this.extractToken()
  }

  authorize() {
    const {
      client_id,
      response_type,
      redirect_uri,
    } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri,
        show_dialog: 'true'
      }
    })

    window.location.href = (`${this.config.url}?${p}`)
  }

  token = null

  extractToken() {
    const rawToken = sessionStorage.getItem('token');
    if (rawToken) {
      this.token = JSON.parse(rawToken)
    }

    if (!this.token && window.location.hash) {
      const p = new HttpParams({
        fromString: window.location.hash
      })
      this.token = p.get('#access_token')
      window.location.hash = ''
      sessionStorage.setItem('token', JSON.stringify(this.token))
    }

  }

  getToken() {
    if (!this.token) { this.authorize() }

    return this.token
  }
}

